# step-form

It's a step form!

## Installation

- Install packages
```
npm init
```

- Install concurrently globally

```
npm i -g concurrently
```

- Run app

```
npm run dev
```