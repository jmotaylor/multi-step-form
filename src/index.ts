import {StepForm} from "./StepForm.js"

const form = document.getElementById('sf__form') as HTMLFormElement
new StepForm(form, {prev: "PREV", withConfirm: true, withProgress: true})