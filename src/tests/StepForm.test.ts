import { StepForm } from "../StepForm"
import { createFormWithSections, appendInputsToSections } from "./testHelpers"
import ButtonStore from "../buttons/Store"
import FormState from "../FormState.js"
import SectionStore from "../sections/Store"

test('Returns the correct number of sections', () => {
  const form = createFormWithSections(5)
  const stepForm = new StepForm(form)

  expect(stepForm.stepContainers.length).toBe(5)
})

test('Text in buttons are correct', () => {
  const form = createFormWithSections()
  new StepForm(
    form,
    { prev: "前へ", next: "次へ", reset: "リセット", submit: "送信" }
  )

  const buttonStore = ButtonStore.getInstance()
  expect(buttonStore.prevBtn?.button.textContent).toBe("前へ")
  expect(buttonStore.nextBtn?.button.textContent).toBe("次へ")
  expect(buttonStore.resetBtn?.button.textContent).toBe("リセット")
  expect(buttonStore.submitBtn?.button.textContent).toBe("送信")
})

test('Text in buttons use default when not declared', () => {
  const form = createFormWithSections()
  new StepForm(form)

  const buttonStore = ButtonStore.getInstance()
  expect(buttonStore.prevBtn?.button.textContent).toBe("Previous")
  expect(buttonStore.nextBtn?.button.textContent).toBe("Next")
  expect(buttonStore.resetBtn?.button.textContent).toBe("Reset")
  expect(buttonStore.submitBtn?.button.textContent).toBe("Submit")
})

test('Can declare only previous button text', () => {
  const form = createFormWithSections()
  new StepForm(form, { prev: "前へ" })

  const buttonStore = ButtonStore.getInstance()
  expect(buttonStore.prevBtn?.button.textContent).toBe("前へ")
  expect(buttonStore.nextBtn?.button.textContent).toBe("Next")
  expect(buttonStore.resetBtn?.button.textContent).toBe("Reset")
  expect(buttonStore.submitBtn?.button.textContent).toBe("Submit")
})

test('Can declare only next button text', () => {
  const form = createFormWithSections()
  new StepForm(form, { next: "次へ" })

  const buttonStore = ButtonStore.getInstance()
  expect(buttonStore.prevBtn?.button.textContent).toBe("Previous")
  expect(buttonStore.nextBtn?.button.textContent).toBe("次へ")
  expect(buttonStore.resetBtn?.button.textContent).toBe("Reset")
  expect(buttonStore.submitBtn?.button.textContent).toBe("Submit")
})

test('Can declare only reset button text', () => {
  const form = createFormWithSections()
  new StepForm(form, { reset: "リセット" })

  const buttonStore = ButtonStore.getInstance()
  expect(buttonStore.prevBtn?.button.textContent).toBe("Previous")
  expect(buttonStore.nextBtn?.button.textContent).toBe("Next")
  expect(buttonStore.resetBtn?.button.textContent).toBe("リセット")
  expect(buttonStore.submitBtn?.button.textContent).toBe("Submit")
})

test('Can declare only submit button text', () => {
  const form = createFormWithSections()
  new StepForm(form, { submit: "送信" })

  const buttonStore = ButtonStore.getInstance()
  expect(buttonStore.prevBtn?.button.textContent).toBe("Previous")
  expect(buttonStore.nextBtn?.button.textContent).toBe("Next")
  expect(buttonStore.resetBtn?.button.textContent).toBe("Reset")
  expect(buttonStore.submitBtn?.button.textContent).toBe("送信")
})

test('Sections except for the first one will have the css class of none', () => {
  const form = createFormWithSections()
  const stepForm = new StepForm(form)

  stepForm.stepContainers.forEach((section, index) => {
    expect(section.classList.contains('none')).toBe(index == 0 ? false : true)
  })
})

test('Sections except for the first one will have the css class of none even with confirmation section', () => {
  FormState.deleteInstance()

  const form = createFormWithSections()
  const stepForm = new StepForm(form, {withConfirm: true})

  stepForm.stepContainers.forEach((section, index) => {
    expect(section.classList.contains('none')).toBe(index == 0 ? false : true)
  })
})

test('Visibility of sections are correct', () => {
  const form = createFormWithSections()
  new StepForm(form)
  
  const sectionStore = SectionStore.getInstance()
  
  sectionStore.sections?.forEach((section, index)=>{
    expect(section.section.classList.contains('none')).toBe(index == 0 ? false : true)
  })

  const buttonStore = ButtonStore.getInstance()
  buttonStore.nextBtn?.button.click()

  sectionStore.sections?.forEach((section, index)=>{
    expect(section.section.classList.contains('none')).toBe(index == 1 ? false : true)
  })

  buttonStore.nextBtn?.button.click()

  sectionStore.sections?.forEach((section, index)=>{
    expect(section.section.classList.contains('none')).toBe(index == 2 ? false : true)
  })
})

test('Visibility of sections are correct with confirmation section', () => {
  SectionStore.deleteInstance()
  FormState.deleteInstance()

  const form = createFormWithSections(2)
  new StepForm(form, {withConfirm: true})
  
  const sectionStore = SectionStore.getInstance()
  
  sectionStore.sections?.forEach((section, index)=>{
    expect(section.section.classList.contains('none')).toBe(index == 0 ? false : true)
  })

  const buttonStore = ButtonStore.getInstance()
  buttonStore.nextBtn?.button.click()

  sectionStore.sections?.forEach((section, index)=>{
    expect(section.section.classList.contains('none')).toBe(index == 1 ? false : true)
  })

  buttonStore.nextBtn?.button.click()

  sectionStore.sections?.forEach((section, index)=>{
    expect(section.section.classList.contains('none')).toBe(index == 2 ? false : true)
  })
})

test('Confirmation section displays the correct info', () => {
  SectionStore.deleteInstance();
  FormState.deleteInstance();
  ButtonStore.deleteInstance();
  
  let form = createFormWithSections();
  form = appendInputsToSections(form);
  document.body.append(form);

  new StepForm(form, {withConfirm: true});
  
  const formState = FormState.getInstance();
  const buttonStore = ButtonStore.getInstance();
  const numberOfInputSections = formState.maxStep - 1;
  
  for (let i = 0; i < numberOfInputSections; i++) {
    buttonStore.nextBtn!.button.click();
  }

  const sectionStore = SectionStore.getInstance()
  const confirmationContainer = sectionStore.getLastSection!.section.firstElementChild;
  
  expect(confirmationContainer!.children[0].textContent).toBe('Name: James Bond');
  expect(confirmationContainer!.children[1].textContent).toBe('Age: 35');
  expect(confirmationContainer!.children[2].textContent).toBe('Drink: Martini (shaken, not stirred)');
})

test('Active element is on first input', () => {
  SectionStore.deleteInstance();
  
  let form: HTMLFormElement = createFormWithSections();
  form = appendInputsToSections(form);
  document.body.append(form);

  new StepForm(form, {withConfirm: true});
  
  const sectionStore = SectionStore.getInstance();
  const storedInput = sectionStore.sections![0].section.firstElementChild!.firstElementChild!.nextElementSibling as HTMLInputElement;
  const documentInput = document.querySelector('input') as HTMLInputElement;

  expect(storedInput).toStrictEqual(documentInput);
  expect(document.activeElement).toStrictEqual(documentInput);
})