import Button from "./Button"

export default class ButtonStore {
  private static instance: ButtonStore | null

  public prevBtn?: Button
  public nextBtn?: Button
  public resetBtn?: Button
  public submitBtn?: Button

  set setPrevBtn(button: Button) {
    this.prevBtn = button
  }

  set setNextBtn(button: Button) {
    this.nextBtn = button
  }

  set setResetBtn(button: Button) {
    this.resetBtn = button
  }

  set setSubmitBtn(button: Button) {
    this.submitBtn = button
  }

  private constructor() {

  }

  static getInstance(): ButtonStore {
    if (this.instance) {
      return this.instance
    }

    this.instance = new ButtonStore()

    return this.instance    
  }

  static deleteInstance() {
    this.instance = null
  }  
}