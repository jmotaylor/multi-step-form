import InputStore from "../inputs/Store.js"
import { FormRegion, FormButton } from "../enums.js"
import FormState from "../FormState.js"
import UpdateUI from "../UpdateUI.js"

export default class Button {
  public button: HTMLButtonElement
  constructor(
    public type: FormButton,
    private label: string | null,
  ) {
    this.button = document.createElement('button')
  }
  
  public generateButtonElement(): HTMLButtonElement {
    switch (this.type) {
      case FormButton.Prev:
        this.button.textContent = this.label ?? "Previous"
        this.button.setAttribute('type', 'button')  
        this.button.setAttribute('id', 'sf__prev-btn') 
        this.button.setAttribute('class', 'hide') 
        this.button.addEventListener('click', this.handleNavigationButtonClicked.bind(this))
        break;

      case FormButton.Next:
        this.button.textContent = this.label ?? "Next"
        this.button.setAttribute('type', 'button')
        this.button.setAttribute('id', 'sf__next-btn') 
        this.button.setAttribute('class', 'show')
        this.button.addEventListener('click', this.handleNavigationButtonClicked.bind(this))
        break;

      case FormButton.Reset:
        this.button.textContent = this.label ?? "Reset";    
        this.button.setAttribute('type', 'button')
        this.button.setAttribute('id', 'sf__reset-btn')
        this.button.setAttribute('class', 'hide') 
        this.button.addEventListener('click', this.handleResetButtonClicked.bind(this))
        break;

      case FormButton.Submit:
        this.button.textContent = this.label ?? "Submit";    
        this.button.setAttribute('type', 'submit')
        this.button.setAttribute('id', 'sf__submit-btn')
        this.button.setAttribute('class', 'hide') 
        this.button.addEventListener('click', this.handleSubmitButtonClicked.bind(this))
        break;
    
      default:
        this.button.textContent = '';
        break;
    }

    return this.button;
  }

  public hideButton(): void {
    this.button.classList.remove('show');
    this.button.classList.add('hide')
  }

  public showButton(): void {
    this.button.classList.remove('hide');
    this.button.classList.add('show');
  }

  private handleNavigationButtonClicked(): void {
    let num;

    switch (this.type) {
      case FormButton.Prev:
        num = -1;
        break;
      
      case FormButton.Next:
        num = 1;
        break;
    
      default:
        num = 0;
        break;
    }

    const formState = FormState.getInstance()

    UpdateUI.updateFormUI(formState.currentStep + num)

    if (formState.region == FormRegion.End && formState.hasConfirmSection) {
      UpdateUI.fillConfirmationSection()
    }
  }

  private handleResetButtonClicked(): void {
    const inputStore = InputStore.getInstance();
    const inputs = inputStore.inputs;

    const formState = FormState.getInstance();
    formState.data = {};

    inputs?.forEach(input => (input.input as HTMLInputElement).value = "")

    UpdateUI.updateFormUI(1);
  }

  private handleSubmitButtonClicked(e: Event) : void {
    e.preventDefault();

    const formState = FormState.getInstance();
    const data = formState.data;
    
    console.log(data);
  }
}