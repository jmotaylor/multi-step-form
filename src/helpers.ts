export async function getTemplate(template: string): Promise<string> {
  return await fetch('./components/' + template)
    .then((response) => { 
      const text = response.text()

      return text
    })
    .then(data => data);
}