import Button from "./buttons/Button.js"
import ButtonStore from "./buttons/Store.js"
import { FormButton } from "./enums.js"
import FormState from "./FormState.js"
import Input from "./inputs/Input.js"
import InputStore from "./inputs/Store.js"
import ProgressBar from "./progress_bar/ProgressBar.js"
import ProgressBarStore from "./progress_bar/Store.js"
import Section from "./sections/Section.js"
import SectionStore from "./sections/Store.js"
import { getTemplate } from "./helpers.js"
import UpdateUI from "./UpdateUI.js"

export class StepForm {
	public stepContainers: Element[];
  public inputs: Element[];
  public labels: Element[];
  public prevBtnTxt: string | null = null;
  public nextBtnTxt: string | null = null;   
  public resetBtnTxt: string | null = null;   
  public submitBtnTxt: string | null = null;   

	constructor(
		public form: HTMLFormElement,
		public options?: { 
			prev?: string,
			next?: string,
			reset?: string,
			submit?: string,
      withConfirm?: boolean,
      withProgress?: boolean,
		}
	) {
		this.stepContainers = Array.from(this.form.getElementsByClassName('sf__step-container'));
    this.inputs = Array.from(this.form.getElementsByClassName('sf__input'));
    this.labels = Array.from(this.form.getElementsByTagName('label'));

    if (options) {
      // Get labels for buttons if specified
			this.prevBtnTxt = options.prev ? options.prev : null;
			this.nextBtnTxt = options.next ? options.next : null;
			this.resetBtnTxt = options.reset ? options.reset : null;
			this.submitBtnTxt = options.submit ? options.submit : null;

      if (options.withConfirm) {
        // Adds a confirmation section if this property is set to true
        const confirmSection = this.createConfirmSection();
        this.stepContainers.push(confirmSection);
      }

      if (options.withProgress) {
        // Adds a confirmation section if this property is set to true
        this.createProgressBar();
      }      
		}

    this.initState();

    const formState = FormState.getInstance();
    formState.setOptions = {
      withConfirm: options?.withConfirm ? options.withConfirm : false,
      withProgress: options?.withProgress ? options.withProgress: false,
    }
	}

  private initState(): void {
    const formState = FormState.getInstance()

    formState.setMaxStep = this.stepContainers.length;
    formState.setHasConfirmSection = !!this.options?.withConfirm;

    this.setupSections();
    this.storeInputs();
    
    this.createButtons(
      this.form,
      {
        prevBtnTxt: this.prevBtnTxt,
        nextBtnTxt: this.nextBtnTxt,
        resetBtnTxt: this.resetBtnTxt,
        submitBtnTxt: this.submitBtnTxt,
      }
    );

    // Set focus on very first input
    const sectionStore = SectionStore.getInstance();
    const firstInput = (sectionStore.sections![0].section.getElementsByClassName('sf__input')[0]);

    if (firstInput) {
      (firstInput as HTMLInputElement).focus();
    }
  }

  private createButtons(form: HTMLFormElement,
		buttons: {
			prevBtnTxt: string | null,
			nextBtnTxt: string | null,
			resetBtnTxt: string | null,
			submitBtnTxt: string | null,
		}): void {
    const prevBtn = new Button(FormButton.Prev, buttons.prevBtnTxt);
    const nextBtn = new Button(FormButton.Next, buttons.nextBtnTxt);
    const resetBtn = new Button(FormButton.Reset, buttons.resetBtnTxt);
    const submitBtn = new Button(FormButton.Submit, buttons.submitBtnTxt);
    form.append(prevBtn.generateButtonElement());
    form.append(nextBtn.generateButtonElement());
    form.append(resetBtn.generateButtonElement());
    form.append(submitBtn.generateButtonElement());
  
    const buttonStore = ButtonStore.getInstance();
    buttonStore.setPrevBtn = prevBtn;
    buttonStore.setNextBtn = nextBtn;   
    buttonStore.setResetBtn = resetBtn;  
    buttonStore.setSubmitBtn = submitBtn;  
  }

  private setupSections(): void {
    const sectionStore = SectionStore.getInstance();

    sectionStore.setSections = this.stepContainers.map((stepContainer: Element, index) => {
      if (index != 0) {
        stepContainer.classList.add('none');
      }

      return new Section(stepContainer);
    })
  }

  public storeInputs(): void {
    const inputStore = InputStore.getInstance();
    
    inputStore.setInputs = this.inputs.map((input: Element) =>  new Input(input));
  }

  private createConfirmSection(): HTMLDivElement {
    const confirmSection = document.createElement('div');
    confirmSection.classList.add('sf__step-container');
    document.getElementById('sf__form')?.append(confirmSection);

    return confirmSection;
  }

  private async createProgressBar(): Promise<void> {
    const progressBar = document.createElement('div');
    progressBar.id = 'sf__progress-bar-container';
    progressBar.innerHTML = await getTemplate('progressBar.html');
    
    const progressBarStore = ProgressBarStore.getInstance();
    progressBarStore.setProgressBar = new ProgressBar(progressBar);
    this.form.insertBefore(progressBar, this.form.querySelector('.sf__step-container'));

    UpdateUI.changeProgressBarLength();
  }
}