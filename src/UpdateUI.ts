
import ButtonStore from "./buttons/Store.js"
import InputStore from "./inputs/Store.js"
import { FormRegion } from "./enums.js"
import Section from "./sections/Section.js"
import SectionStore from "./sections/Store.js"
import FormState from "./FormState.js";
import ProgressBarStore from "./progress_bar/Store.js";

export default class UpdateUI {
  private constructor() {}

  static changeProgressBarLength(): void {
    const formState = FormState.getInstance();

    if (formState.options.withProgress) {
      const percentage = (formState.currentStep / formState.maxStep) * 100;
      
      const progressBarStore = ProgressBarStore.getInstance();
      progressBarStore.progressBar!.innerBar.style.width = percentage + '%';
    }
  }

  static updateFormUI(currentStep: number) {
    const formState = FormState.getInstance()
    const sectionStore = SectionStore.getInstance()
  
    formState.changeCurrentStep(currentStep)
    this.updateSection(sectionStore.sections, formState.currentStep)
    this.updateButtonVisibility(formState)
    this.changeProgressBarLength();
  }

  static updateSection(sections: Section[] | undefined, currentStep: number): void {
    sections?.forEach(section => section.hide())
  
    if (sections) {
      sections[currentStep - 1].show()
    }
  }

  static updateButtonVisibility(state: FormState): void {
    const buttonStore = ButtonStore.getInstance()
  
    switch (state.region) {
      case FormRegion.Start :
        buttonStore.prevBtn?.hideButton()
        buttonStore.nextBtn?.showButton()
        buttonStore.resetBtn?.hideButton()
        buttonStore.submitBtn?.hideButton()
        break
  
      case FormRegion.Middle :
        buttonStore.prevBtn?.showButton()
        buttonStore.nextBtn?.showButton()
        buttonStore.resetBtn?.hideButton()
        buttonStore.submitBtn?.hideButton()
        break
  
      case FormRegion.End :
        buttonStore.prevBtn?.showButton()
        buttonStore.nextBtn?.hideButton()
        buttonStore.resetBtn?.showButton()
        buttonStore.submitBtn?.showButton()
        break
    
      default:
        break
    }  
  }

  static fillConfirmationSection(): void {
    document.getElementById('sf__confirmation-container')?.remove(); // Reset confirmation seciton

    const inputStore = InputStore.getInstance();
    const sectionStore = SectionStore.getInstance();
    const confirmSection = sectionStore.getLastSection?.section;

    const confirmationContainer = document.createElement('div');
    confirmationContainer.id = 'sf__confirmation-container';
    
    const formState = FormState.getInstance();

    inputStore.inputs?.forEach((input) => {
      const label = input.label;
      const value = (input.input as HTMLInputElement | HTMLTextAreaElement).value.trim();
      
      const entry = document.createElement('div');
      entry.textContent = `${label}: ${value}`;

      formState.data[input.name!] = value;
      confirmationContainer.append(entry);
    })

    confirmSection?.append(confirmationContainer);
  }  
}