import Section from "./Section"

export default class SectionStore {
  private static instance: SectionStore | null

  public sections?: Section[]

  set setSections(sections: Section[]) {
    this.sections = sections
  }

  get getLastSection(): Section | null {
    if (this.sections) {
       return this.sections[this.sections.length -1]
    } else {
      return null
    }
  }

  private constructor() {}

  static getInstance(): SectionStore {
    if (this.instance) {
      return this.instance
    }

    this.instance = new SectionStore()

    return this.instance    
  }

  static deleteInstance() {
    this.instance = null
  }
}