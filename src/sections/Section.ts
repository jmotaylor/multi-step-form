export default class Section {
  constructor(public section: Element) {}

  public show() {
    this.section.classList.remove('none')
  }

  public hide() {
    this.section.classList.add('none')
  }
}