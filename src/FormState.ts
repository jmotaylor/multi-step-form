import { FormRegion } from "./enums.js"

export default class FormState {
  public currentStep: number = 1;
  public maxStep: number = 1;
  public region: FormRegion = FormRegion.Start;
  public hasConfirmSection = false;
  public data:{[key:string]: string | boolean} = {};
  public options = {
    withConfirm: false,
    withProgress: false
  };

  private static instance: FormState | null

  set setMaxStep(maxStep: number) {
    this.maxStep = maxStep
  }

  set setHasConfirmSection(flag: boolean) {
    this.hasConfirmSection = flag
  }

  set setOptions(options: { withConfirm: boolean, withProgress: boolean }) {
    this.options = options;
  }
  
  private constructor() {}

  static getInstance(): FormState {
    if (this.instance) {
      return this.instance
    }

    this.instance = new FormState()

    return this.instance
  }

  static deleteInstance() {
    this.instance = null
  }

  public changeCurrentStep(newStep: number): void {
    if (newStep >= 1 && newStep <= this.maxStep) {
      this.currentStep = newStep
  
      this.changeFormRegion()
    }
  }

  private changeFormRegion(): void {
    if (this.currentStep == this.maxStep) {
      this.region = FormRegion.End
    }

    if (this.currentStep == 1) {
      this.region = FormRegion.Start
    }

    if (this.currentStep != 1 && this.currentStep != this.maxStep) {
      this.region = FormRegion.Middle
    }
  }
}