export default class ProgressBar {
  public innerBar: HTMLDivElement;

  constructor(public element: Element) {
    this.innerBar = element.querySelector('.sf__progress-bar-inner') as HTMLDivElement;
  }
}