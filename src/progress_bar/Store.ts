import ProgressBar from "./ProgressBar";

export default class ProgressBarStore {
  private static instance: ProgressBarStore | null;

  public progressBar?: ProgressBar;

  set setProgressBar(progressBar: ProgressBar) {
    this.progressBar = progressBar;
  }  

  private constructor() {}

  static getInstance(): ProgressBarStore {
    if (this.instance) {
      return this.instance;
    }

    this.instance = new ProgressBarStore();

    return this.instance;    
  }

  static deleteInstance() {
    this.instance = null;
  }  
}