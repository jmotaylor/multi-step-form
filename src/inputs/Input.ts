import { InputType } from "../enums.js";

export default class Input {
  public type: InputType | null = null;
  public label: string | null = null;
  public name: string | null = null;

  constructor(
    public input: Element,
  ) {
    this.assignType();
    this.assignLabel();
    this.assignName();
  }

  private assignType() {
    switch (this.input.tagName) {
      case 'INPUT':
        this.type = InputType.Input;
        break;
      case 'TEXTAREA':
        this.type = InputType.Textarea;
        break;
    
      default:
        break;
    }
  }

  private assignLabel() {
    const id = this.input.id;
    const labelElement = document.querySelector(`[for="${id}"]`);

    if (labelElement) {
      this.label = labelElement.textContent;
    }
  }

  private assignName() {
    const name = this.input.getAttribute('name');

    if (name) {
      this.name = name;
    } else {
      throw new Error(`Input/Textarea with id of ${this.input.id} does not have a name attribute`);
    }
  }
}