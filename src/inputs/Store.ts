import Input from "./Input"

export default class InputStore {
  private static instance: InputStore | null;

  public inputs?: Input[];

  set setInputs(inputs: Input[]) {
    this.inputs = inputs;
  }

  private constructor() {}

  static getInstance(): InputStore {
    if (this.instance) {
      return this.instance;
    }

    this.instance = new InputStore();

    return this.instance;    
  }

  static deleteInstance() {
    this.instance = null;
  }
}