import InputStore from "../inputs/Store.js";
import { FormRegion, FormButton } from "../enums.js";
import FormState from "../FormState.js";
import UpdateUI from "../UpdateUI.js";
export default class Button {
    constructor(type, label) {
        this.type = type;
        this.label = label;
        this.button = document.createElement('button');
    }
    generateButtonElement() {
        var _a, _b, _c, _d;
        switch (this.type) {
            case FormButton.Prev:
                this.button.textContent = (_a = this.label) !== null && _a !== void 0 ? _a : "Previous";
                this.button.setAttribute('type', 'button');
                this.button.setAttribute('id', 'sf__prev-btn');
                this.button.setAttribute('class', 'hide');
                this.button.addEventListener('click', this.handleNavigationButtonClicked.bind(this));
                break;
            case FormButton.Next:
                this.button.textContent = (_b = this.label) !== null && _b !== void 0 ? _b : "Next";
                this.button.setAttribute('type', 'button');
                this.button.setAttribute('id', 'sf__next-btn');
                this.button.setAttribute('class', 'show');
                this.button.addEventListener('click', this.handleNavigationButtonClicked.bind(this));
                break;
            case FormButton.Reset:
                this.button.textContent = (_c = this.label) !== null && _c !== void 0 ? _c : "Reset";
                this.button.setAttribute('type', 'button');
                this.button.setAttribute('id', 'sf__reset-btn');
                this.button.setAttribute('class', 'hide');
                this.button.addEventListener('click', this.handleResetButtonClicked.bind(this));
                break;
            case FormButton.Submit:
                this.button.textContent = (_d = this.label) !== null && _d !== void 0 ? _d : "Submit";
                this.button.setAttribute('type', 'submit');
                this.button.setAttribute('id', 'sf__submit-btn');
                this.button.setAttribute('class', 'hide');
                this.button.addEventListener('click', this.handleSubmitButtonClicked.bind(this));
                break;
            default:
                this.button.textContent = '';
                break;
        }
        return this.button;
    }
    hideButton() {
        this.button.classList.remove('show');
        this.button.classList.add('hide');
    }
    showButton() {
        this.button.classList.remove('hide');
        this.button.classList.add('show');
    }
    handleNavigationButtonClicked() {
        let num;
        switch (this.type) {
            case FormButton.Prev:
                num = -1;
                break;
            case FormButton.Next:
                num = 1;
                break;
            default:
                num = 0;
                break;
        }
        const formState = FormState.getInstance();
        UpdateUI.updateFormUI(formState.currentStep + num);
        if (formState.region == FormRegion.End && formState.hasConfirmSection) {
            UpdateUI.fillConfirmationSection();
        }
    }
    handleResetButtonClicked() {
        const inputStore = InputStore.getInstance();
        const inputs = inputStore.inputs;
        const formState = FormState.getInstance();
        formState.data = {};
        inputs === null || inputs === void 0 ? void 0 : inputs.forEach(input => input.input.value = "");
        UpdateUI.updateFormUI(1);
    }
    handleSubmitButtonClicked(e) {
        e.preventDefault();
        const formState = FormState.getInstance();
        const data = formState.data;
        console.log(data);
    }
}
//# sourceMappingURL=Button.js.map