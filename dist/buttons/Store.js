export default class ButtonStore {
    constructor() {
    }
    set setPrevBtn(button) {
        this.prevBtn = button;
    }
    set setNextBtn(button) {
        this.nextBtn = button;
    }
    set setResetBtn(button) {
        this.resetBtn = button;
    }
    set setSubmitBtn(button) {
        this.submitBtn = button;
    }
    static getInstance() {
        if (this.instance) {
            return this.instance;
        }
        this.instance = new ButtonStore();
        return this.instance;
    }
    static deleteInstance() {
        this.instance = null;
    }
}
//# sourceMappingURL=Store.js.map