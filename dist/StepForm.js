var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import Button from "./buttons/Button.js";
import ButtonStore from "./buttons/Store.js";
import { FormButton } from "./enums.js";
import FormState from "./FormState.js";
import Input from "./inputs/Input.js";
import InputStore from "./inputs/Store.js";
import ProgressBar from "./progress_bar/ProgressBar.js";
import ProgressBarStore from "./progress_bar/Store.js";
import Section from "./sections/Section.js";
import SectionStore from "./sections/Store.js";
import { getTemplate } from "./helpers.js";
import UpdateUI from "./UpdateUI.js";
export class StepForm {
    constructor(form, options) {
        this.form = form;
        this.options = options;
        this.prevBtnTxt = null;
        this.nextBtnTxt = null;
        this.resetBtnTxt = null;
        this.submitBtnTxt = null;
        this.stepContainers = Array.from(this.form.getElementsByClassName('sf__step-container'));
        this.inputs = Array.from(this.form.getElementsByClassName('sf__input'));
        this.labels = Array.from(this.form.getElementsByTagName('label'));
        if (options) {
            this.prevBtnTxt = options.prev ? options.prev : null;
            this.nextBtnTxt = options.next ? options.next : null;
            this.resetBtnTxt = options.reset ? options.reset : null;
            this.submitBtnTxt = options.submit ? options.submit : null;
            if (options.withConfirm) {
                const confirmSection = this.createConfirmSection();
                this.stepContainers.push(confirmSection);
            }
            if (options.withProgress) {
                this.createProgressBar();
            }
        }
        this.initState();
        const formState = FormState.getInstance();
        formState.setOptions = {
            withConfirm: (options === null || options === void 0 ? void 0 : options.withConfirm) ? options.withConfirm : false,
            withProgress: (options === null || options === void 0 ? void 0 : options.withProgress) ? options.withProgress : false,
        };
    }
    initState() {
        var _a;
        const formState = FormState.getInstance();
        formState.setMaxStep = this.stepContainers.length;
        formState.setHasConfirmSection = !!((_a = this.options) === null || _a === void 0 ? void 0 : _a.withConfirm);
        this.setupSections();
        this.storeInputs();
        this.createButtons(this.form, {
            prevBtnTxt: this.prevBtnTxt,
            nextBtnTxt: this.nextBtnTxt,
            resetBtnTxt: this.resetBtnTxt,
            submitBtnTxt: this.submitBtnTxt,
        });
        const sectionStore = SectionStore.getInstance();
        const firstInput = (sectionStore.sections[0].section.getElementsByClassName('sf__input')[0]);
        if (firstInput) {
            firstInput.focus();
        }
    }
    createButtons(form, buttons) {
        const prevBtn = new Button(FormButton.Prev, buttons.prevBtnTxt);
        const nextBtn = new Button(FormButton.Next, buttons.nextBtnTxt);
        const resetBtn = new Button(FormButton.Reset, buttons.resetBtnTxt);
        const submitBtn = new Button(FormButton.Submit, buttons.submitBtnTxt);
        form.append(prevBtn.generateButtonElement());
        form.append(nextBtn.generateButtonElement());
        form.append(resetBtn.generateButtonElement());
        form.append(submitBtn.generateButtonElement());
        const buttonStore = ButtonStore.getInstance();
        buttonStore.setPrevBtn = prevBtn;
        buttonStore.setNextBtn = nextBtn;
        buttonStore.setResetBtn = resetBtn;
        buttonStore.setSubmitBtn = submitBtn;
    }
    setupSections() {
        const sectionStore = SectionStore.getInstance();
        sectionStore.setSections = this.stepContainers.map((stepContainer, index) => {
            if (index != 0) {
                stepContainer.classList.add('none');
            }
            return new Section(stepContainer);
        });
    }
    storeInputs() {
        const inputStore = InputStore.getInstance();
        inputStore.setInputs = this.inputs.map((input) => new Input(input));
    }
    createConfirmSection() {
        var _a;
        const confirmSection = document.createElement('div');
        confirmSection.classList.add('sf__step-container');
        (_a = document.getElementById('sf__form')) === null || _a === void 0 ? void 0 : _a.append(confirmSection);
        return confirmSection;
    }
    createProgressBar() {
        return __awaiter(this, void 0, void 0, function* () {
            const progressBar = document.createElement('div');
            progressBar.id = 'sf__progress-bar-container';
            progressBar.innerHTML = yield getTemplate('progressBar.html');
            const progressBarStore = ProgressBarStore.getInstance();
            progressBarStore.setProgressBar = new ProgressBar(progressBar);
            this.form.insertBefore(progressBar, this.form.querySelector('.sf__step-container'));
            UpdateUI.changeProgressBarLength();
        });
    }
}
//# sourceMappingURL=StepForm.js.map