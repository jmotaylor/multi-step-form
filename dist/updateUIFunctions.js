import ButtonStore from "./buttons/Store.js";
import { FormRegion } from "./enums.js";
import SectionStore from "./sections/Store.js";
import FormState from "./FormState.js";
import ProgressBarStore from "./progress_bar/Store.js";
export function changeProgressBarLength() {
    const formState = FormState.getInstance();
    const percentage = (formState.currentStep / formState.maxStep) * 100;
    const progressBarStore = ProgressBarStore.getInstance();
    progressBarStore.progressBar.innerBar.style.width = percentage + '%';
}
export function updateFormUI(currentStep) {
    const formState = FormState.getInstance();
    const sectionStore = SectionStore.getInstance();
    formState.changeCurrentStep(currentStep);
    updateSection(sectionStore.sections, formState.currentStep);
    updateButtonVisibility(formState);
}
export function updateSection(sections, currentStep) {
    sections === null || sections === void 0 ? void 0 : sections.forEach(section => section.hide());
    if (sections) {
        sections[currentStep - 1].show();
    }
}
export function updateButtonVisibility(state) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;
    const buttonStore = ButtonStore.getInstance();
    switch (state.region) {
        case FormRegion.Start:
            (_a = buttonStore.prevBtn) === null || _a === void 0 ? void 0 : _a.hideButton();
            (_b = buttonStore.nextBtn) === null || _b === void 0 ? void 0 : _b.showButton();
            (_c = buttonStore.resetBtn) === null || _c === void 0 ? void 0 : _c.hideButton();
            (_d = buttonStore.submitBtn) === null || _d === void 0 ? void 0 : _d.hideButton();
            break;
        case FormRegion.Middle:
            (_e = buttonStore.prevBtn) === null || _e === void 0 ? void 0 : _e.showButton();
            (_f = buttonStore.nextBtn) === null || _f === void 0 ? void 0 : _f.showButton();
            (_g = buttonStore.resetBtn) === null || _g === void 0 ? void 0 : _g.hideButton();
            (_h = buttonStore.submitBtn) === null || _h === void 0 ? void 0 : _h.hideButton();
            break;
        case FormRegion.End:
            (_j = buttonStore.prevBtn) === null || _j === void 0 ? void 0 : _j.showButton();
            (_k = buttonStore.nextBtn) === null || _k === void 0 ? void 0 : _k.hideButton();
            (_l = buttonStore.resetBtn) === null || _l === void 0 ? void 0 : _l.showButton();
            (_m = buttonStore.submitBtn) === null || _m === void 0 ? void 0 : _m.showButton();
            break;
        default:
            break;
    }
}
//# sourceMappingURL=updateUIFunctions.js.map