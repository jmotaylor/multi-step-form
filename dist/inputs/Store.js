export default class InputStore {
    constructor() { }
    set setInputs(inputs) {
        this.inputs = inputs;
    }
    static getInstance() {
        if (this.instance) {
            return this.instance;
        }
        this.instance = new InputStore();
        return this.instance;
    }
    static deleteInstance() {
        this.instance = null;
    }
}
//# sourceMappingURL=Store.js.map