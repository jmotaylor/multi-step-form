import { InputType } from "../enums.js";
export default class Input {
    constructor(input) {
        this.input = input;
        this.type = null;
        this.label = null;
        this.name = null;
        this.assignType();
        this.assignLabel();
        this.assignName();
    }
    assignType() {
        switch (this.input.tagName) {
            case 'INPUT':
                this.type = InputType.Input;
                break;
            case 'TEXTAREA':
                this.type = InputType.Textarea;
                break;
            default:
                break;
        }
    }
    assignLabel() {
        const id = this.input.id;
        const labelElement = document.querySelector(`[for="${id}"]`);
        if (labelElement) {
            this.label = labelElement.textContent;
        }
    }
    assignName() {
        const name = this.input.getAttribute('name');
        if (name) {
            this.name = name;
        }
        else {
            throw new Error(`Input/Textarea with id of ${this.input.id} does not have a name attribute`);
        }
    }
}
//# sourceMappingURL=Input.js.map