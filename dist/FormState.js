import { FormRegion } from "./enums.js";
export default class FormState {
    constructor() {
        this.currentStep = 1;
        this.maxStep = 1;
        this.region = FormRegion.Start;
        this.hasConfirmSection = false;
        this.data = {};
        this.options = {
            withConfirm: false,
            withProgress: false
        };
    }
    set setMaxStep(maxStep) {
        this.maxStep = maxStep;
    }
    set setHasConfirmSection(flag) {
        this.hasConfirmSection = flag;
    }
    set setOptions(options) {
        this.options = options;
    }
    static getInstance() {
        if (this.instance) {
            return this.instance;
        }
        this.instance = new FormState();
        return this.instance;
    }
    static deleteInstance() {
        this.instance = null;
    }
    changeCurrentStep(newStep) {
        if (newStep >= 1 && newStep <= this.maxStep) {
            this.currentStep = newStep;
            this.changeFormRegion();
        }
    }
    changeFormRegion() {
        if (this.currentStep == this.maxStep) {
            this.region = FormRegion.End;
        }
        if (this.currentStep == 1) {
            this.region = FormRegion.Start;
        }
        if (this.currentStep != 1 && this.currentStep != this.maxStep) {
            this.region = FormRegion.Middle;
        }
    }
}
//# sourceMappingURL=FormState.js.map