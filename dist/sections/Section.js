export default class Section {
    constructor(section) {
        this.section = section;
    }
    show() {
        this.section.classList.remove('none');
    }
    hide() {
        this.section.classList.add('none');
    }
}
//# sourceMappingURL=Section.js.map