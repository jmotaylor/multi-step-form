import { StepForm } from "../StepForm";
import { createFormWithSections, appendInputsToSections } from "./testHelpers";
import ButtonStore from "../buttons/Store";
import SectionStore from "../sections/Store";
import InputStore from "../inputs/Store";
import FormState from "../FormState";
test('Initial visibility of buttons are correct', () => {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    new StepForm(createFormWithSections());
    const buttonStore = ButtonStore.getInstance();
    expect((_a = buttonStore.prevBtn) === null || _a === void 0 ? void 0 : _a.button.classList.contains('show')).toBe(false);
    expect((_b = buttonStore.prevBtn) === null || _b === void 0 ? void 0 : _b.button.classList.contains('hide')).toBe(true);
    expect((_c = buttonStore.nextBtn) === null || _c === void 0 ? void 0 : _c.button.classList.contains('show')).toBe(true);
    expect((_d = buttonStore.nextBtn) === null || _d === void 0 ? void 0 : _d.button.classList.contains('hide')).toBe(false);
    expect((_e = buttonStore.resetBtn) === null || _e === void 0 ? void 0 : _e.button.classList.contains('show')).toBe(false);
    expect((_f = buttonStore.resetBtn) === null || _f === void 0 ? void 0 : _f.button.classList.contains('hide')).toBe(true);
    expect((_g = buttonStore.submitBtn) === null || _g === void 0 ? void 0 : _g.button.classList.contains('show')).toBe(false);
    expect((_h = buttonStore.submitBtn) === null || _h === void 0 ? void 0 : _h.button.classList.contains('hide')).toBe(true);
});
test('Visibility of buttons update correctly', () => {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t;
    new StepForm(createFormWithSections());
    const buttonStore = ButtonStore.getInstance();
    (_a = buttonStore.nextBtn) === null || _a === void 0 ? void 0 : _a.button.click();
    expect((_b = buttonStore.prevBtn) === null || _b === void 0 ? void 0 : _b.button.classList.contains('show')).toBe(true);
    expect((_c = buttonStore.prevBtn) === null || _c === void 0 ? void 0 : _c.button.classList.contains('hide')).toBe(false);
    expect((_d = buttonStore.nextBtn) === null || _d === void 0 ? void 0 : _d.button.classList.contains('show')).toBe(true);
    expect((_e = buttonStore.nextBtn) === null || _e === void 0 ? void 0 : _e.button.classList.contains('hide')).toBe(false);
    expect((_f = buttonStore.resetBtn) === null || _f === void 0 ? void 0 : _f.button.classList.contains('show')).toBe(false);
    expect((_g = buttonStore.resetBtn) === null || _g === void 0 ? void 0 : _g.button.classList.contains('hide')).toBe(true);
    expect((_h = buttonStore.submitBtn) === null || _h === void 0 ? void 0 : _h.button.classList.contains('show')).toBe(false);
    expect((_j = buttonStore.submitBtn) === null || _j === void 0 ? void 0 : _j.button.classList.contains('hide')).toBe(true);
    (_k = buttonStore.nextBtn) === null || _k === void 0 ? void 0 : _k.button.click();
    expect((_l = buttonStore.prevBtn) === null || _l === void 0 ? void 0 : _l.button.classList.contains('show')).toBe(true);
    expect((_m = buttonStore.prevBtn) === null || _m === void 0 ? void 0 : _m.button.classList.contains('hide')).toBe(false);
    expect((_o = buttonStore.nextBtn) === null || _o === void 0 ? void 0 : _o.button.classList.contains('show')).toBe(false);
    expect((_p = buttonStore.nextBtn) === null || _p === void 0 ? void 0 : _p.button.classList.contains('hide')).toBe(true);
    expect((_q = buttonStore.resetBtn) === null || _q === void 0 ? void 0 : _q.button.classList.contains('show')).toBe(true);
    expect((_r = buttonStore.resetBtn) === null || _r === void 0 ? void 0 : _r.button.classList.contains('hide')).toBe(false);
    expect((_s = buttonStore.submitBtn) === null || _s === void 0 ? void 0 : _s.button.classList.contains('show')).toBe(true);
    expect((_t = buttonStore.submitBtn) === null || _t === void 0 ? void 0 : _t.button.classList.contains('hide')).toBe(false);
});
test('Visibility of buttons update correctly with confirmation section', () => {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t;
    FormState.deleteInstance();
    new StepForm(createFormWithSections(2), { withConfirm: true });
    const buttonStore = ButtonStore.getInstance();
    (_a = buttonStore.nextBtn) === null || _a === void 0 ? void 0 : _a.button.click();
    expect((_b = buttonStore.prevBtn) === null || _b === void 0 ? void 0 : _b.button.classList.contains('show')).toBe(true);
    expect((_c = buttonStore.prevBtn) === null || _c === void 0 ? void 0 : _c.button.classList.contains('hide')).toBe(false);
    expect((_d = buttonStore.nextBtn) === null || _d === void 0 ? void 0 : _d.button.classList.contains('show')).toBe(true);
    expect((_e = buttonStore.nextBtn) === null || _e === void 0 ? void 0 : _e.button.classList.contains('hide')).toBe(false);
    expect((_f = buttonStore.resetBtn) === null || _f === void 0 ? void 0 : _f.button.classList.contains('show')).toBe(false);
    expect((_g = buttonStore.resetBtn) === null || _g === void 0 ? void 0 : _g.button.classList.contains('hide')).toBe(true);
    expect((_h = buttonStore.submitBtn) === null || _h === void 0 ? void 0 : _h.button.classList.contains('show')).toBe(false);
    expect((_j = buttonStore.submitBtn) === null || _j === void 0 ? void 0 : _j.button.classList.contains('hide')).toBe(true);
    (_k = buttonStore.nextBtn) === null || _k === void 0 ? void 0 : _k.button.click();
    expect((_l = buttonStore.prevBtn) === null || _l === void 0 ? void 0 : _l.button.classList.contains('show')).toBe(true);
    expect((_m = buttonStore.prevBtn) === null || _m === void 0 ? void 0 : _m.button.classList.contains('hide')).toBe(false);
    expect((_o = buttonStore.nextBtn) === null || _o === void 0 ? void 0 : _o.button.classList.contains('show')).toBe(false);
    expect((_p = buttonStore.nextBtn) === null || _p === void 0 ? void 0 : _p.button.classList.contains('hide')).toBe(true);
    expect((_q = buttonStore.resetBtn) === null || _q === void 0 ? void 0 : _q.button.classList.contains('show')).toBe(true);
    expect((_r = buttonStore.resetBtn) === null || _r === void 0 ? void 0 : _r.button.classList.contains('hide')).toBe(false);
    expect((_s = buttonStore.submitBtn) === null || _s === void 0 ? void 0 : _s.button.classList.contains('show')).toBe(true);
    expect((_t = buttonStore.submitBtn) === null || _t === void 0 ? void 0 : _t.button.classList.contains('hide')).toBe(false);
});
test('Attributes of buttons are correct', () => {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    new StepForm(createFormWithSections());
    const buttonStore = ButtonStore.getInstance();
    expect((_a = buttonStore.prevBtn) === null || _a === void 0 ? void 0 : _a.button.id).toBe('sf__prev-btn');
    expect((_b = buttonStore.nextBtn) === null || _b === void 0 ? void 0 : _b.button.id).toBe('sf__next-btn');
    expect((_c = buttonStore.resetBtn) === null || _c === void 0 ? void 0 : _c.button.id).toBe('sf__reset-btn');
    expect((_d = buttonStore.submitBtn) === null || _d === void 0 ? void 0 : _d.button.id).toBe('sf__submit-btn');
    expect((_e = buttonStore.prevBtn) === null || _e === void 0 ? void 0 : _e.button.type).toBe('button');
    expect((_f = buttonStore.nextBtn) === null || _f === void 0 ? void 0 : _f.button.type).toBe('button');
    expect((_g = buttonStore.resetBtn) === null || _g === void 0 ? void 0 : _g.button.type).toBe('button');
    expect((_h = buttonStore.submitBtn) === null || _h === void 0 ? void 0 : _h.button.type).toBe('submit');
});
test('Form resets when reset button is clicked', () => {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
    SectionStore.deleteInstance();
    FormState.deleteInstance();
    ButtonStore.deleteInstance();
    let form = createFormWithSections();
    form = appendInputsToSections(form);
    new StepForm(form, { withConfirm: true });
    const formState = FormState.getInstance();
    const buttonStore = ButtonStore.getInstance();
    const numberOfInputSections = formState.maxStep - 1;
    for (let i = 0; i < numberOfInputSections; i++) {
        (_a = buttonStore.nextBtn) === null || _a === void 0 ? void 0 : _a.button.click();
    }
    (_b = buttonStore.resetBtn) === null || _b === void 0 ? void 0 : _b.button.click();
    const inputStore = InputStore.getInstance();
    const inputs = inputStore.inputs;
    inputs === null || inputs === void 0 ? void 0 : inputs.forEach((input) => {
        expect(input.input.value).toBe("");
    });
    expect(formState.currentStep).toBe(1);
    const sectionStore = SectionStore.getInstance();
    (_c = sectionStore.sections) === null || _c === void 0 ? void 0 : _c.forEach((section, index) => {
        index == 0
            ? expect(section.section.classList.contains('none')).toBe(false)
            : expect(section.section.classList.contains('none')).toBe(true);
    });
    expect((_d = buttonStore.prevBtn) === null || _d === void 0 ? void 0 : _d.button.classList.contains('show')).toBe(false);
    expect((_e = buttonStore.prevBtn) === null || _e === void 0 ? void 0 : _e.button.classList.contains('hide')).toBe(true);
    expect((_f = buttonStore.nextBtn) === null || _f === void 0 ? void 0 : _f.button.classList.contains('show')).toBe(true);
    expect((_g = buttonStore.nextBtn) === null || _g === void 0 ? void 0 : _g.button.classList.contains('hide')).toBe(false);
    expect((_h = buttonStore.resetBtn) === null || _h === void 0 ? void 0 : _h.button.classList.contains('show')).toBe(false);
    expect((_j = buttonStore.resetBtn) === null || _j === void 0 ? void 0 : _j.button.classList.contains('hide')).toBe(true);
    expect((_k = buttonStore.submitBtn) === null || _k === void 0 ? void 0 : _k.button.classList.contains('show')).toBe(false);
    expect((_l = buttonStore.submitBtn) === null || _l === void 0 ? void 0 : _l.button.classList.contains('hide')).toBe(true);
});
//# sourceMappingURL=Button.test.js.map