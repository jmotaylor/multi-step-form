var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { getTemplate } from "../helpers.js";
import ProgressBar from "../progress_bar/ProgressBar";
import ProgressBarStore from "../progress_bar/Store";
import UpdateUI from "../UpdateUI";
export function createFormWithSections(numOfSections = 3, withProgressBar = false) {
    const form = document.createElement('form');
    for (let i = 0; i < numOfSections; i++) {
        const section = document.createElement('div');
        section.classList.add('sf__step-container');
        form.append(section);
    }
    if (withProgressBar) {
        createProgressBar(form);
    }
    document.body.append(form);
    return form;
}
export function appendInputsToSections(form) {
    const stepContainers = Array.from(form.getElementsByClassName('sf__step-container'));
    if (stepContainers.length != 3) {
        throw Error('Form must contain 3 step containers');
    }
    stepContainers.forEach((stepContainer, index) => {
        switch (index) {
            case 0:
                stepContainer.append(buildInputGroup('name', 'text', 'James Bond', true));
                break;
            case 1:
                stepContainer.append(buildInputGroup('age', 'number', '35', true));
                break;
            case 2:
                stepContainer.append(buildInputGroup('drink', 'text', 'Martini (shaken, not stirred)', true));
                break;
            default:
                stepContainer.append();
                break;
        }
        form.append(stepContainer);
    });
    return form;
}
function buildInputGroup(id, type, value, withValues) {
    const inputContainer = document.createElement('div');
    const label = document.createElement('label');
    const labelText = id.charAt(0).toUpperCase() + id.slice(1);
    label.setAttribute('for', id);
    label.textContent = labelText;
    const input = document.createElement('input');
    input.setAttribute('type', type);
    input.id = id;
    input.classList.add('sf__input');
    input.setAttribute('name', id);
    if (withValues) {
        input.value = value;
    }
    inputContainer.append(label);
    inputContainer.append(input);
    return inputContainer;
}
function createProgressBar(form) {
    return __awaiter(this, void 0, void 0, function* () {
        const progressBar = document.createElement('div');
        progressBar.id = 'sf__progress-bar-container';
        progressBar.innerHTML = yield getTemplate('progressBar.html');
        console.log('template: ', yield getTemplate('progressBar.html'));
        const progressBarStore = ProgressBarStore.getInstance();
        progressBarStore.setProgressBar = new ProgressBar(progressBar);
        form.insertBefore(progressBar, form.querySelector('.sf__step-container'));
        UpdateUI.changeProgressBarLength();
    });
}
//# sourceMappingURL=testHelpers.js.map