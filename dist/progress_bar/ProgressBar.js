export default class ProgressBar {
    constructor(element) {
        this.element = element;
        this.innerBar = element.querySelector('.sf__progress-bar-inner');
    }
}
//# sourceMappingURL=ProgressBar.js.map