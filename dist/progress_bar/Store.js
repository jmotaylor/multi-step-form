export default class ProgressBarStore {
    constructor() { }
    set setProgressBar(progressBar) {
        this.progressBar = progressBar;
    }
    static getInstance() {
        if (this.instance) {
            return this.instance;
        }
        this.instance = new ProgressBarStore();
        return this.instance;
    }
    static deleteInstance() {
        this.instance = null;
    }
}
//# sourceMappingURL=Store.js.map